
package bondarenko.vladimir.task4.domain;

public interface UserDao extends DAO<Users, Integer> {

    Users getByUsername(String username);
    
}
