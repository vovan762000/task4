package bondarenko.vladimir.task4.web;

import bondarenko.vladimir.task4.domain.Users;
import bondarenko.vladimir.task4.domain.UsersDaoImpl;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AuthServlet", urlPatterns = {"/login","/auth"})
public class AuthServlet extends HttpServlet {

    UsersDaoImpl users = new UsersDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("auth.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String page = "";
        if (!isEmpty(username) && !isEmpty(password)) {

            List<Users> userList = users.getList();
            for (Users user : userList) {
                if (user.getPassword().equals(password) && user.getNickname().equals(username)) {
                    Cookie auth = new Cookie("my_app_auth", String.valueOf(System.currentTimeMillis()));
                    auth.setMaxAge(600);
                    resp.addCookie(auth);
                    page = "protected";
                    break;
                } else {
                    page = "register";
                }
            }
            resp.sendRedirect(page);
        }
    }

    private boolean isEmpty(String value) {
        if (value == null) {
            return true;
        }
        value = value.trim();
        return !(value.length() > 0);
    }

}
