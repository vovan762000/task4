package bondarenko.vladimir.task4.web;

import bondarenko.vladimir.task4.domain.Users;
import bondarenko.vladimir.task4.domain.UsersDaoImpl;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    UsersDaoImpl users = new UsersDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req, resp);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firsname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String nickname = req.getParameter("nickname");
        String password = req.getParameter("password");
        users.create(new Users(nickname, password, firsname, lastname));
        Cookie auth = new Cookie("my_app_auth", String.valueOf(System.currentTimeMillis()));
        auth.setMaxAge(600);
        resp.addCookie(auth);
        resp.sendRedirect("protected");
    }

}
