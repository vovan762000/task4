package bondarenko.vladimir.task4.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "RequestFilter", urlPatterns = {"/protected"})
public class RequestFilter implements Filter {
    
    public RequestFilter() {
    }
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        Cookie appCookie = getCookie(request);
        if (appCookie == null) {
            ((HttpServletResponse)response).sendRedirect("auth.jsp");
            
        } else {
            if (appCookie.getValue().isEmpty()) {
                ((HttpServletResponse)response).sendRedirect("auth.jsp");
            }
        }
        chain.doFilter(request, response);
    }
    
    public void destroy() {
    }
    
    public void init(FilterConfig filterConfig) {
        
    }

    private Cookie getCookie(ServletRequest request) {
        Cookie[] cookies = ((HttpServletRequest) request).getCookies();
        for (Cookie cooky : cookies) {
            if (cooky.getName().equals("my_app_auth")) {
                return cooky;
            }
        }
        return null;
    }
    
}
